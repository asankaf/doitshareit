﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSH.Access.CommentsAccess.Model
{
    public class NewCommentInfo
    {
        public int CommentId;
        public int ParentId;
        public string Body;
    }
}
