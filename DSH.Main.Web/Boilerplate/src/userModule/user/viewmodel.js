﻿define(['Boiler', '../../Models/Suser'], function (Boiler, sUser) {

    var viewModel = function (context) {
        var self = this;
        self.users = ko.observableArray();
        // self.posts = ko.observableArray();
        // self.loadPostUrl = "/Post/GetPost";
        // self.loadCommentUrl = "/Comment/Index";


        self.display = function () {
            self.users([]);
            $.ajax({
                type: "GET",
                url: "/User/AllUsers",
                data: {},
                success: function (result) {
                    if (result.Status == "SUCCESS") {
                        var allUsers = result.Result.Data;
                        for (var i = 0; i < allUsers.length; i++) {
                            var xuser = new sUser(self.moduleContext);
                           // xuser.id(allUsers[i].Id);
                            xuser.DisplayName(allUsers[i].DisplayName);
                            xuser.Avatar(allUsers[i].PicLocation);
                            xuser.Reputation(allUsers[i].Reputation);
                            self.users.push(xuser);
                        }


                    }
                }
            });
        }
    };

    return viewModel;
});
