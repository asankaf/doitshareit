﻿define(['Boiler', './settings', './user/component'], function (Boiler, settings, Suser) {

    var Module = function (globalContext) {

        var context = new Boiler.Context(globalContext);
        context.addSettings(settings);

        //the landing page should respond to the root URL, so let's use an URLController toop
        var controller = new Boiler.UrlController($(".appcontent"));
        controller.addRoutes({
            "/suser" : new Suser(context)
        });
        controller.start();

    };

    return Module;

});