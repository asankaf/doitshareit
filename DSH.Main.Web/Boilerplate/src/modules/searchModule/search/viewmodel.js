﻿define(['Boiler'], function (Boiler) {

    var ViewModel = function (moduleContext) {

        $(document).ready(function () {

//            $('#name-list').focus(function () {
//                $('#newsfeedlink').click();
//                Boiler.UrlController.goTo("");
//            });


            $('#name-list').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/Home/Searchuser",
                        data: { searchText: request.term, maxResults: 10 },
                        dataType: "json",
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    value: item.DisplayName,
                                    avatar: item.PicLocation,
                                    rep: item.Reputation,
                                    selectedId: item.Id
                                };
                            }));
                        }
                    });
                },
                select: function (event, ui) {

                    Boiler.UrlController.goTo("user/" + ui.item.selectedId);
                    $('#name-list').val("");
                    return false;
                }

            }).data("autocomplete")._renderItem = function (ul, item) {
                $(ul).css({
                    "height": "auto",
                    "display": "block",
                    "position": "fixed",
                    "left": "282px", //457
                    "top": "38px", //38
                    "width": "379px"

                });
                var inner_html = '<a><div class="list_item_container"><div class="image"><img src="' + item.avatar + '"></div><div class="label"><h4>' + item.label + '</h4></div><div class="description"><h4>' + item.rep + ' Reputation</h4></hr></div></div></a>';
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append(inner_html)
                    .appendTo(ul);

            };

        });

    };


    return ViewModel;

});


 
