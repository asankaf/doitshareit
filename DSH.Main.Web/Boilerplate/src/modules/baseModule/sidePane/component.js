define(['Boiler', './viewmodel', 'text!./view.html'], function (Boiler, ViewModel, template) {

    var Component = function (moduleContext) {

        var vm, panel = null;
        this.activate = function (parent, params) {
            if (!panel) {
                panel = new Boiler.ViewTemplate(parent, template, null);
                vm = new ViewModel(moduleContext);
                ko.applyBindings(vm, panel.getDomElement());
                vm.loadFrequentUsers();
                vm.loadFrequentUsers();
                vm.loadReputation();
            }

            panel.show();
        };

        this.deactivate = function () {
            if (panel) {
                panel.hide();
            }

        };

    };

    // need to add bit of css styling into js 
    // I know this is bad, I tried everting elese.

    //    $(function () {


    //        var right_pos = $("#newsfeed_wall").offset().left;
    //        var container_width = $("#newsfeed_wall").width();

    //        right_pos += container_width + 30;

    //        console.log("r" + right_pos);
    //        console.log("w" + container_width);

    //        $(".right_aside").css("left", right_pos + 100);

    //    });







    return Component;

}); 