﻿define(['Boiler'], function (Boiler) {
    var viewModel = function (moduleContext, id) {
        var self = this;
        var isAnonymous = false;

        //  var selected_id = 0;

        this.postText = ko.observable("");

        this.picLocation = ko.observable("");
        this.titleText = ko.observable("");

        // getting the profile picture url from the display module
        this.picUrl = moduleContext.retreiveObject("profilePicURL");

        //        this.anonymousAvater = "../../Boilerplate/src/modules/baseModule/theme/gray/bullet1.png";
        this.anonymousAvater = "../../Content/unknown.jpg";

        // the posting panel will show the pic of you as default: ( default is non anonymous posting) 
        this.picLocation(this.picUrl);

        this.makePost = function (formElement) {
            console.log("Manoj " + id);
            if (formElement.elements["post"].value == '') {
                //alert("Empty Comment is posted!!");
                console.log("Empty comment was posted!");

                $.msgBox({
                    title: "MyRetro",
                    content: "You cannot post empty feedback",
                    type: "error",
                    buttons: [{ value: "Ok"}],
                    afterShow: function (result) { }
                });

            } else {
                var msg = " ";
                if (isAnonymous) {

                    msg = "Your feedback is anonymous. Confirm to send.";

                } else {

                    msg = "Your feedback is NOT anonymous. It will appear on NewsFeed. Confirm to send.";

                }

                $.msgBox({
                    title: "Are You Sure",
                    content: msg,
                    type: "confirm",
                    buttons: [{ value: "Send it!" }, { value: "Don't Send"}],
                    success: function (result) {
                        if (result == "Send it!") {
                            ////////////////////////////////////////////////////////////////////////////
                            $.ajax({
                                type: "POST",
                                url: "/Post/Create",
                                dataType: "json",
                                data: {
                                    Body: $('<div/>').text(formElement.elements["post"].value).html(),
                                    PostTypeId: 2,
                                    TaggedUserId: id,
                                    IsAnonymous: isAnonymous
                                },
                                success: function (result) {
                                    if (result.Status == "SUCCESS") {
                                        self.postText("");
                                        moduleContext.notify('NEW_POST', result.Result.Data);
                                        // location.reload();
                                    }

                                    if (result.Status == "ANONYMOUS_SUCCESS") {
                                        // moduleContext.notify('NEW_POST', result.Result.Data);
                                        self.postText("");

                                        $.msgBox({
                                            title: "MyRetro",
                                            content: "Successfully sent your anonymous feedback!",
                                            type: "info"
                                        });

                                    }
                                }
                            });

                            /////////////////////////////////////////////////////////////////////////////////////







                        }
                    }
                });







            }



        };

        this.anonymousToggle = function (clickedElement) {

          
            if (isAnonymous) {
                isAnonymous = false;
                self.picLocation(self.picUrl);
                self.titleText("known");
                $("#lab2").text("testingggg");
            } else {
                isAnonymous = true;
                self.picLocation(self.anonymousAvater);
                self.titleText("anonymous");
             //   alert("anonymous");
                $("#lab2").text("okkkk");
            }
        };
    };

    return viewModel;
});
