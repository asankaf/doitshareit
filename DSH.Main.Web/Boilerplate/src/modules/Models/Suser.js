﻿define([], function () {

    var user = function(context) {
        var self = this;
        self.id = "";
        self.DisplayName = ko.observable("");
        self.Avatar = ko.observable("");
        self.Reputation = ko.observable("");
        
    };
    return user;
});