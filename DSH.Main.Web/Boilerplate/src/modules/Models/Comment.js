﻿define(['Boiler'], function (Boiler) {

    var comment = function(context) {
        var self = this;
        self.id = "";
        self.body = ko.observable();
        self.score = ko.observable(0);
        self.picUrl = ko.observable("");
        self.ownerDisplayName = ko.observable("");
        self.OwnerUser = "";
      

        self.voteUpComment = function() {
            $.ajax({
                async: false,
                type: "GET",
                url: "/Vote/UpVoteComment",
                data: { commentId: self.id },
                success: function(result) {
                    if (result.Status == "SUCCESS") {
                        self.score(result.Result);
                    } else {
                     
                        $.msgBox({
                            title: "MyRetro",
                            content: result.Result,
                            type: "error"
                        });

                    }
                }
            });
        };


        self.goToCommentedUser = function(){
            console.log("userId is: " + self.OwnerUser);
            if (self.OwnerUser != null)
                Boiler.UrlController.goTo("user/" + self.OwnerUser);
        };


    };
    return comment;
});