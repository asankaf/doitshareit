﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>MyRetro</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Addding new items from our lold index.aspx to here -->



<!--<link rel="icon" href="favicon.ico" type="image/x-icon">-->
<!--<link href="../../Boilerplate/src/modules/baseModule/theme/gray/reset.css" rel="stylesheet" type="text/css" />-->
<link href="../../Boilerplate/src/modules/baseModule/theme/style/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" type="text/css" id="themeStylesheet" href="../../Boilerplate/src/modules/baseModule/theme/gray/common.css" �>-->

<link href="../../Boilerplate/src/modules/baseModule/theme/gray/main.css" rel="stylesheet"
    type="text/css" />
<link href="../../Boilerplate/src/modules/baseModule/theme/gray/normalize.css" rel="stylesheet"
    type="text/css" />


<link href="../../Boilerplate/libs/wijmo/jquery-wijmo.css" rel="stylesheet" type="text/css" title="rocket-jqueryui" />
   
<link href="../../Content/css/msgBoxLight.css" rel="stylesheet" type="text/css" /> 
<link rel="icon" type="image/png" href="../../Content/logo_title.png">
<link href="../../Content/themes/base/jquery.ui.autocomplete.css" rel="stylesheet" type="text/css" /> 

<link href="../../Boilerplate/src/modules/baseModule/theme/style/custom.css" rel="stylesheet"
    type="text/css" />
  

      
<style type="text/css" media="screen">
            
    a:hover {
        color: #33348e;
        text-decoration: none;
        cursor: inherit
    }

    .ui-dialog-titlebar { visibility: hidden; }

    #msgbox {

        background-color: #FF6600;
        color: white;
        text-align: center;
    }
    
    #logout 
    {
        color: rgb(49, 47, 47);
        position: relative;
        left: 126px;
        top: -20px;
        font-size: 12px;
    }
    
    #logout:hover {
        color:orangered;
        text-decoration: underline;
    } 
       
       
             
</style>

<!-- end of pasting old code for the header -->

<link rel="stylesheet" type="text/css" href="../../Content/css/site-forms.css" />
<link rel="stylesheet" type="text/css" href="../../Content/css/site-grids.css" />
<link rel="stylesheet" type="text/css" href="../../Content/css/site-pages.css" />

</head>

<body>
    <script src="../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>

     <script>
         $(document).ready(function () {
             checkCookie();
         });

         function getCookie(c_name) {
             var c_value = document.cookie;
             var c_start = c_value.indexOf(" " + c_name + "=");
             if (c_start == -1) {
                 c_start = c_value.indexOf(c_name + "=");
             }
             if (c_start == -1) {
                 c_value = null;
             }
             else {
                 c_start = c_value.indexOf("=", c_start) + 1;
                 var c_end = c_value.indexOf(";", c_start);
                 if (c_end == -1) {
                     c_end = c_value.length;
                 }
                 c_value = unescape(c_value.substring(c_start, c_end));
             }
             return c_value;
         }

         function setCookie(c_name, value, exdays) {
             var exdate = new Date();
             exdate.setDate(exdate.getDate() + exdays);
             var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
             document.cookie = c_name + "=" + c_value;
         }

         function checkCookie() {
             var username = getCookie("username");
             if (username != null && username != "") {
                 $("#hint").css("display", "none");
             }
             else {
                // username = prompt("Please enter your name:", "");
                   setCookie("username", "manoj", 365);
                   $("#hint").css({
                       "position" : "fixed",
                       "left": "39%",
                       "top": "35px",
                       "z-index": "10",
                       "margin" : "0 auto",
                       "display" : "inline"

                   });

                   $("#hint").click(function () {
                       $("#hint").fadeOut();
                   });
                

             }
         }
     </script>
    
   <div>
   
     
       </div>

    <div id="page-content">
		<header>
            <div id="testing" class="clearfix">
		        <div class="header clearfix">

		            <div id="logo2">       
                        <a href="#"><img src="../../Content/Images/newlogolarge.png"  style=" height: 38px;"/></a>
		            </div>

		            <div id="searchArea">
		                <ul class="clearfix">
		                    <li class="notification">
		                        <!--<div class="icoGlobe"></div>-->
		                    </li>
		                    <li class="search">
		                        <!--<input type="text" class="txtSearch" />
		                        <a href="#" class="btnSearch"></a></li>-->
		                    <li class="user">
		                        <!--<div class="userThumb"></div>-->
                                <!--<a  id="logout" onclick="logMeOut()" style=" " >Logout</a>-->
                        
		                    </li>
                    
                            <li class="main-menu"></li>

                            <li class="logoutButton" onclick="logMeOut()">Logout</li>
		         
		                </ul>
	                </div>
            
            
                        <div id="dialog_box" style="display:none">
                            <p>You will also be logged out from your Linkedin account.</p>
                            <p>Do you want to continue ?</p>
                        </div>
	            </div>
            </div>
        </header>	
			<%--<section class="notification"></section>
            <section class="search" style="font-size:25px;position:relative;left:400px"></section>
              
              <section class="user" ></section>
             
              <a class="logout" >Logout</a>
              
              

              <div id="Div1" style="display: none;">
                    <p>You will also be logged out from your Linkedin account.</p>
                    <p>Do you want to continue ?</p>
              </div>--%>
            
            
        <div id="homepageWrapper">
            <div class="content">
                <div class="content_body clearfix">
		    
                    <section class="main-content">
                        <div class="posting_panel_holder"></div>
                        <div class="appcontent">
                              <div id="hint" style="display:none;">
                                  <img src="../../Content/hint.png" />
                        </div>
                    </section>
                    <aside class="right_aside">
                        <section class="side-pane"></section>
                    </aside>   

				
                
                </div>
            </div>
        </div>
    </div>

</div>
        
        <script src="../../Boilerplate/libs/jquery/jquery-min.js" type="text/javascript"></script>
        <script>window.jQuery || document.write('<script src="../../Boilerplate/libs/jquery/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="../../Boilerplate/libs/jquery/plugins.js"></script>
        <script src="../../Boilerplate/libs/jquery/main.js"></script>

        <script src="../../Boilerplate/libs/wijmo/jquery-ui.min.js" type="text/javascript"></script>
        
        <script src="../../Boilerplate/libs/xing-wysihtml5-fb0cfe4/parser_rules/advanced.js" type="text/javascript"></script>
        <script src="../../Boilerplate/libs/xing-wysihtml5-fb0cfe4/dist/wysihtml5-0.3.0.min.js" type="text/javascript"></script>
        
        <script src="../../Scripts/jquery.msgBox.js" type="text/javascript"></script> 

        <script type="text/javascript" src="http://platform.linkedin.com/in.js">
            api_key: <%=ConfigurationManager.AppSettings["ApiKey"]%>
            authorize: true
        </script>
     
        <script type="text/javascript">

            function logMeOut() {

                $.msgBox({
                    title: "Are you sure?",
                    content: "You will also be logged out from your Linkedin account. Do you want to continue ?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No"}],
                    success: function (result) {
                        if (result == "Yes") {
                            IN.User.logout(loginOut);
                        }
                    }
                });

                //IN.User.logout(loginOut);
            }

            function loginOut() {
                window.location.href = '/Home/Logout';

            }
        </script>
 
		<script src="../../Boilerplate/libs/knockout/knockout-2.1.0pre.js" type="text/javascript" charset="utf-8"></script>
		<script src="../../Boilerplate/libs/underscore/underscore-1.3.3.js" type="text/javascript" charset="utf-8"></script>
		<script src="../../Boilerplate/libs/signals/signals.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../../Boilerplate/libs/crossroads/crossroads.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../../Boilerplate/libs/hasher/hasher.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../../Boilerplate/libs/pubsub/pubsub-20120708.js" type="text/javascript" charset="utf-8"></script>
	    <script src="../../Boilerplate/libs/flot/jquery.flot.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../../Boilerplate/libs/flot/jquery.flot.resize.js" type="text/javascript" charset="utf-8"></script>
		<script src="../../Boilerplate/libs/amplifystore/amplify.store.min.1.1.0.js" type="text/javascript" charset="utf-8"></script>
		<script src="../../Boilerplate/libs/boilerplate/groundwork.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../Boilerplate/libs/moment/moment.js" type="text/javascript" charset="utf-8"></script>
        
        <script type="text/javascript" data-main="../../Boilerplate/src/main" src="../../Boilerplate/libs/require/require.js"></script>


</body>
</html>
