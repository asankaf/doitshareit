﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>MyRetro</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    <%--<link href="../../Boilerplate/src/modules/baseModule/theme/gray/reset.css" rel="stylesheet"
        type="text/css" />--%>
    <link href="../../Boilerplate/src/modules/baseModule/theme/style/style.css" rel="stylesheet"
        type="text/css" />


    <link href="../../Boilerplate/src/modules/baseModule/theme/gray/main.css" rel="stylesheet"
        type="text/css" />
    <link href="../../Boilerplate/src/modules/baseModule/theme/gray/normalize.css" rel="stylesheet"
        type="text/css" />
    <link href="../../Boilerplate/src/modules/baseModule/theme/style/custom.css" rel="stylesheet"
        type="text/css" />
        
    <link rel="icon" type="image/png" href="../../Content/logo_title.png">
   
    
    <script src="../../Boilerplate/libs/jquery/vendor/modernizr-2.6.2.min.js"></script>

 
    
</head>
<body id="login_page_body">
   
   <div id="mainWrapper">
       <!-- <div class="login_logo"> -->
       <!--<div class="login_logo"> </div> -->
       <div id="logoHeader">
           <img src="../../Boilerplate/src/modules/baseModule/theme/login/newlogolarge.png" alt="MyRetro" style="width: 266px;" /> 
       </div>
        <!-- 3. Displays a button to let the viewer authenticate -->
        <!--<div class="login_button_box rounded-corners"> -->

        <!-- 4. Placeholder for the greeting -->
        <!--<div id="profiles"></div>-->
   
        <div id="contentWrapper">
            

            <div class="login_footer loginShadow">
                <h2>Welcome to MyRetro.</h2>  
                <p> Our careers are well dependent on constructive critical feedback. We shouldn't be limited to the yearend reviews, but embrace continuous appraisals throughout. </p>   
                <p>MyRetro is a system where you can keep a record of your feedback to other individuals(positive/constructive), and also receive feedback from the peers</p>
                <p>Importantly, it provides a completely "anonumous" feedback mechanism. Don't worry,  we are very clear on this and do not keep identity information even on server memory.</p>
                
                <div id="linkedin" class="login_button">
        
                        <script  type="IN/Login" ></script>
        
                </div>
            </div>

            


        </div>

        <div id="footer_bottom_div">
            <p id="footer_bottom">© 2013 99X Technology. All rights reserved</p>
        </div>
   </div>
   


   <script src="../../Boilerplate/libs/jquery/jquery-min.js" type="text/javascript"></script>
   <script>window.jQuery || document.write('<script src="../../Boilerplate/libs/jquery/vendor/jquery-1.10.2.min.js"><\/script>')</script>
   <script src="../../Boilerplate/libs/jquery/plugins.js"></script>
   <script src="../../Boilerplate/libs/jquery/
in.js"></script>

    <script type="text/javascript">

        var width = $("html").width();
        //        console.log(width);
        if (width > 1422) {
            // if width > 1422 then remove the backgroud image
            $("#login_page_body").css();
        }

    </script>
     <script>
         $(function () {
             $('#linkedin')
                 .css({ display: 'none' });
         });
     </script>
 
     <script type="text/javascript" src="http://platform.linkedin.com/in.js">
         api_key: <%=ConfigurationManager.AppSettings["ApiKey"]%>
         authorize: false
         onLoad: onLinkedInLoad
     </script>
    
        <script type="text/javascript">
            // 2. Runs when the JavaScript framework is loaded
            function onLinkedInLoad() {
                $('a[id*=li_ui_li_gen_]').css({margin:'5px'}).html('<input type="button" id="linkedin_login" value="Login with LinkedIn" />'); 
                $('#linkedin')
                    .css({display:'block'});
                IN.Event.on(IN, "auth", onLinkedInAuth);
            }

            // 2. Runs when the viewer has authenticated
            function onLinkedInAuth() {
                IN.API.Profile("me")
                    .fields("id","firstName", "lastName","pictureUrl", "publicProfileUrl")
                    .result(displayProfiles);
            }

            // 2. Runs when the Profile() API call returns successfully
            function displayProfiles(profiles) {
                member = profiles.values[0];
                // document.getElementById("profiles").innerHTML =
                // "<p id=\"" + member.id + "\">Hello " + member.firstName + " " + member.lastName +" "+member.publicProfileUrl+ "</p>";

                $.ajax({
                    cache: false,
                    type: "POST",
                    url: "Home/login",
                    data: { UserUniqueid:member.id, DisplayName: member.firstName +" "+ member.lastName, PublicProfileUrl:member.publicProfileUrl,PicLocation:member.pictureUrl },
                    
                }).done( function (data) {
                    document.location.href = 'Home/Index';
                    
                });
            }

        </script>
</body>
</html>



